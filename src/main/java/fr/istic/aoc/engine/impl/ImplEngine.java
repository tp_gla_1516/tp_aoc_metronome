package fr.istic.aoc.engine.impl;

import fr.istic.aoc.command.Command;
import fr.istic.aoc.command.TypeEvent;
import fr.istic.aoc.controller.Clock;
import fr.istic.aoc.controller.impl.ImplClock;
import fr.istic.aoc.engine.Engine;

import java.util.HashMap;

/**
 * Created by lautre on 23/10/15.
 */
public class ImplEngine implements Engine {

    private HashMap<TypeEvent, Command> commands = new HashMap<>();
    private int bpm;
    private int mesure;
    private Clock clock = new ImplClock();

    @Override
    public int getBPM() {
        return bpm;
    }

    @Override
    public void setBPM(int bpm) {
        if(bpm<=300 && bpm>=20 && bpm !=this.bpm) {
            //modification si necessaire
            this.bpm = bpm;
            //Modification du tempo dans la clock
            clock.setBPM(this.bpm);
        }
        //notification du controleur (Mise à jour IHM)
        commands.get(TypeEvent.UPDATE_BPM).execute();

    }

    @Override
    public int getMesure() {
        return mesure;
    }

    @Override
    public void setMesure(int mesure) {
        if(mesure>=2 && mesure<=7 && mesure != this.mesure) this.mesure = mesure;
        commands.get(TypeEvent.UPDATE_MESURE).execute();

    }

    @Override
    public void addCommand(TypeEvent typeEvent,Command command){
        commands.put(typeEvent,command);
    }

    @Override
    public void stop() {
        clock.stop();
    }

    @Override
    public void start() {
        clock.start();
    }

    @Override
    public void addClockCommand(TypeEvent typeEvent, Command command) {
        clock.addCommand(typeEvent,command);
    }
}
