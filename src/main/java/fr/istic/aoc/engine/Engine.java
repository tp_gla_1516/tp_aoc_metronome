package fr.istic.aoc.engine;

import fr.istic.aoc.command.Command;
import fr.istic.aoc.command.TypeEvent;

/**
 * Created by guerin on 23/10/15.
 */
public interface Engine {

    /**
     * Gets bpm.
     *
     * @return bpm
     */
    int getBPM();

    /**
     * Sets bpm.
     * Set le BPM et appel l'update du BPM sur l'IHM
     * @param bpm the bpm
     */
    void setBPM(int bpm);

    /**
     * Gets mesure.
     *
     * @return the mesure
     */
    int getMesure();

    /**
     * Sets mesure.
     * Set la mesure et appel l'update de la mesure sur l'IHM
     * @param mesure the mesure
     */
    void setMesure(int mesure);

    /**
     * Add command.
     * Ajoute une commande a la liste des commandes disponible
     * @param typeEvent the type event
     * @param command   the command
     */
    void addCommand(TypeEvent typeEvent, Command command);

    /**
     * Stop la clock.
     */
    void stop();

    /**
     * Start la clock.
     */
    void start();

    /**
     * Add clock command.
     * Ajoute une commande à la liste des commandes disponible pour la clock
     * @param tick    the tick
     * @param command the command
     */
    void addClockCommand(TypeEvent tick, Command command);
}
