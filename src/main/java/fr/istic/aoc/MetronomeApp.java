package fr.istic.aoc;

/**
 * Created by lautre on 09/11/15.
 */

import fr.istic.aoc.command.TypeEvent;
import fr.istic.aoc.controller.Controller;
import fr.istic.aoc.controller.impl.ControllerImpl;
import fr.istic.aoc.engine.Engine;
import fr.istic.aoc.engine.impl.ImplEngine;
import fr.istic.aoc.view.FxController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MetronomeApp extends Application {

    Controller controller;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("/Metronome.fxml").openStream());
        Scene scene= new Scene(root, 600, 300);
        primaryStage.setTitle("Metronome - Laureline/Gweltaz");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.getRoot().requestFocus();

        FxController viewController = fxmlLoader.getController();

        Engine engine = new ImplEngine();
        this.controller = new ControllerImpl(viewController);
        controller.setEngine(engine);

        //Ajout des Commande pour l'engine et la clock
        engine.addClockCommand(TypeEvent.TICK, controller::tick);
        engine.addCommand(TypeEvent.UPDATE_BPM, controller::updateBPM);
        engine.addCommand(TypeEvent.UPDATE_MESURE, controller::updateMesure);

        //Ajout des Commande pour la vue
        viewController.addCommand(TypeEvent.START, controller::startEngine);
        viewController.addCommand(TypeEvent.STOP, controller::stopEngine);
        viewController.addCommand(TypeEvent.SET_BPM, controller::setBPM);
        viewController.addCommand(TypeEvent.SET_MESURE,controller::setMesure);

        //Mise en place des defaults
        viewController.executeCommand(TypeEvent.SET_BPM);
        viewController.executeCommand(TypeEvent.SET_MESURE);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        controller.stopEngine();
        System.exit(0);
    }
}
