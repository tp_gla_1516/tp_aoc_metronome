package fr.istic.aoc.command;

/**
 * Created by guerin on 23/10/15.
 */
public enum TypeEvent {

    START,  //Start Engine
    STOP,   //Stop Engine
    UPDATE_BPM, //Update BPM et Mesure dans le vue
    UPDATE_MESURE,
    SET_BPM,    //Set BPM et Mesure dans l'Engine
    SET_MESURE,
    TICK    //Appelé par la clock Clock
}
