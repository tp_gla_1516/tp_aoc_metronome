package fr.istic.aoc.command;

/**
 * Created by guerin on 23/10/15.
 */
public interface Command {
    /**
     * Execute the command.
     */
    void execute();
}
