package fr.istic.aoc.view;

import fr.istic.aoc.command.Command;
import fr.istic.aoc.command.TypeEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.media.AudioClip;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lautre on 09/11/15.
 */
public class FxController implements Initializable {

    private final static int DEFAULT_BPM = 120;
    private final static int DEFAULT_MESURE = 2;
    private final static Paint ACTIVE_COLOR = Paint.valueOf("7FDD4C");
    private final static Paint DEFAULT_COLOR = Paint.valueOf("808080");

    @FXML private ToggleButton btnStartStop;
    @FXML private Circle ledMesure;
    @FXML private Circle ledBPM;
    @FXML private Slider sliderTempo;
    @FXML private Label textBPM;
    @FXML private Button btnIncMesure;
    @FXML private Button btnDecMesure;
    private AudioClip sonBMP;
    private AudioClip sonMesure;
    private HashMap<TypeEvent, Command> commands = new HashMap<>();

    private int mesure = DEFAULT_MESURE;
    private int bpm = DEFAULT_BPM;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Je suis initialisé");
        sliderTempo.setValue(DEFAULT_BPM);
        sonBMP = new AudioClip(getClass().getResource("/fichierAudio/bpm.wav").toString());
        sonMesure = new AudioClip(getClass().getResource("/fichierAudio/mesure.wav").toString());
    }

    @FXML public void toogleState(ActionEvent actionEvent) {
        if (btnStartStop.getText().equals("ON")){
            btnStartStop.setText("OFF");
            commands.get(TypeEvent.START).execute();
        }
        else{
            btnStartStop.setText("ON");
            commands.get(TypeEvent.STOP).execute();
        }
    }

    @FXML public void ledOn(boolean mesure) {
        ledOnAux(ledBPM,sonBMP);
        if (mesure) ledOnAux(ledMesure,sonMesure);
    }

    private void ledOnAux(Circle led, AudioClip son){
        led.setFill(ACTIVE_COLOR);
        son.play();
        Timer timerLedBPM = new Timer();

        timerLedBPM.schedule(new TimerTask() {
            @Override
            public void run() {
                led.setFill(DEFAULT_COLOR);
            }
        }, 100);
    }

    @FXML public void incMesure(ActionEvent actionEvent) {
        this.mesure++;
        commands.get(TypeEvent.SET_MESURE).execute();
    }

    @FXML public void decMesure(ActionEvent actionEvent) {
        this.mesure--;
        commands.get(TypeEvent.SET_MESURE).execute();
    }

    public void setMesure(int mesure) {
        this.mesure = mesure;
        System.out.println("UpdateMesureFXController : " + this.mesure);
    }

    public void setBPM(int bpm) {
        this.bpm = bpm;
        textBPM.setText(String.valueOf(bpm));
    }

    public int getMesure() {
        return mesure;
    }

    public int getBpm() {
        return bpm;
    }


    public void addCommand(TypeEvent typeEvent,Command command){
        commands.put(typeEvent,command);
    }

    public void executeCommand(TypeEvent typeEvent) {
        commands.get(typeEvent);
    }

    public void changeTempo(Event event) {
        bpm = (int) sliderTempo.getValue();
        commands.get(TypeEvent.SET_BPM).execute();
    }
}
