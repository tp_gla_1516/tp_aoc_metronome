package fr.istic.aoc.controller;

import fr.istic.aoc.engine.Engine;

/**
 * Created by guerin on 23/10/15.
 */
public interface Controller {


    /**
     * Sets engine.
     *
     * @param engine the engine
     */
    void setEngine(Engine engine);

    /**
     * Update bpm.
     * Est appellée par la commande UPDATE_BPM, met le BPM à jours sur l'IHM
     * Met le BPM à jours sur l'IHM avec le BPM de l'engine
     */
    void updateBPM();

    /**
     * Update mesure.
     * Est appellée par la commande UPDATE_MESURE
     * Met la mesure à jours dans la view avec la mesure de l'engine
     */
    void updateMesure();

    /**
     * Stop engine.
     * Est appellée par la commande STOP par la vue.
     */
    void stopEngine();

    /**
     * Start engine.
     * Est appellée par la commande START par la vue.
     */
    void startEngine();

    /**
     * Tick.
     * Est appellée par la commande TICK appellée par la CLock.
     * Compte le nombre de Tick et Notifie la vue en conséquence Allumage des leds (BPM et/ou Mesure)
     */
    void tick();

    /**
     * Set bpm.
     * Est appellée par la commande SET_BPM par la vue.
     * Set le BPM dans l'engine avec le contenu de la vue.
     */
    void setBPM();

    /**
     * Set mesure.
     * Est appellée par la commande SET_MESURE par la vue.
     * Set la Mesure dans l'engine avec le contenu de la vue.
     */
    void setMesure();
}
