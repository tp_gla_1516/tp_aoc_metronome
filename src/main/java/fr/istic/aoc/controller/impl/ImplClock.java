package fr.istic.aoc.controller.impl;

import fr.istic.aoc.command.Command;
import fr.istic.aoc.command.TypeEvent;
import fr.istic.aoc.controller.Clock;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by lautre on 23/10/15.
 */
public class ImplClock implements Clock {

    private boolean running=false;
    private int bpm;
    private HashMap<TypeEvent, Command> commands = new HashMap<>();
    private ScheduledExecutorService threadPool = Executors.newSingleThreadScheduledExecutor();

    @Override
    public void start() {
        threadPool.scheduleAtFixedRate(commands.get(TypeEvent.TICK)::execute, 0, 60000/bpm, TimeUnit.MILLISECONDS);
        running=true;
    }

    @Override
    public void stop() {
        threadPool.shutdownNow();
        threadPool = Executors.newSingleThreadScheduledExecutor();
        running=false;
    }

    @Override
    public void setBPM(int bpm) {
        this.bpm = bpm;
        if (running) {
            stop();
            start();
        }
    }

    @Override
    public void addCommand(TypeEvent typeEvent, Command command) {
        commands.put(typeEvent,command);
    }
}
