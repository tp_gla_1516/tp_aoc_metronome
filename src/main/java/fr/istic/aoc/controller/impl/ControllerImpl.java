package fr.istic.aoc.controller.impl;

import fr.istic.aoc.controller.Controller;
import fr.istic.aoc.view.FxController;
import fr.istic.aoc.engine.Engine;

/**
 * Created by guerin on 23/10/15.
 */
public class ControllerImpl implements Controller {

    private FxController fxController;
    private Engine engine;
    private int nbTick = 0;

    public ControllerImpl(FxController fxController) {
        this.fxController = fxController;
    }

    @Override //Mise à jours du tempo (appelée par l'ihm)
    public void setBPM(){
        int bpm = fxController.getBpm();
        if(bpm<=300 && bpm>=20) engine.setBPM(bpm);
    }

    @Override //Mise à jours de la mesure (appelée par l'ihm)
    public void setMesure(){
        int mesure = fxController.getMesure();
        if(mesure>=2 && mesure<=7) engine.setMesure(mesure);
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void updateBPM(){
        fxController.setBPM(engine.getBPM());
        System.out.println("UpdateBPM : " + engine.getBPM());
    }

    @Override
     public void updateMesure(){
        fxController.setMesure(engine.getMesure());
    }

    @Override //Stop the engine
    public void stopEngine() {
        engine.stop();
    }

    @Override //Start the engine
    public void startEngine() {
        engine.start();
    }

    @Override
    public void tick() {
        //Incrément du compteur de tick pour savoir ou en est la mesure.
        nbTick++;
        boolean mesureTick = nbTick >= engine.getMesure();
        //Test sur le compteur de tick pour savoir si on doit ticker la mesure.
        if (mesureTick) {
            //Remise du compteur à 0
            nbTick = 0;
        }
        //Mise a jour de la vue
        fxController.ledOn(mesureTick);
    }

}
