package fr.istic.aoc.controller;

import fr.istic.aoc.command.Command;
import fr.istic.aoc.command.TypeEvent;

/**
 * Created by guerin on 23/10/15.
 */
public interface Clock {

    /**
     * Sets bpm.
     *
     * @param bpm the bpm
     */
    void setBPM(int bpm);

    /**
     * Add command.
     * Ajoute une commande a la liste des commande disponible
     * @param typeEvent the type event
     * @param command   the command
     */
    void addCommand(TypeEvent typeEvent,Command command);

    /**
     * Stop.
     * arrete le thread de la clock et le recré
     */
    void stop();

    /**
     * Start.
     * Lance le thread de la clock
     */
    void start();
}
